﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TypeWiseReport.aspx.cs" Inherits="DiagnosticManagement.UserInterface.TypeWiseReport" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
       <title>Care BITM|Type Wise Report</title>
    <link href="Style/Theme.css" rel="stylesheet" />
</head>
<body style="background-image: url('../resourses/image7.jpg')">
    <form id="form1" runat="server">
    <p class="p">BITM Care Bill Management System</p>
    <div class="box">
        <table>
            <tr>
                <td class="auto-style2">
                    <asp:Button ID="home_Button" PostBackUrl="../UserInterface/Home.aspx" CssClass="button" runat="server" Text="Home" Width="80px" style="text-align:left;"/>
                </td>
                <td class="auto-style2">
                    <asp:Button ID="testTypesetup_Button" PostBackUrl="../UserInterface/TestTypeSetup.aspx" CssClass="button"  runat="server" href="TestTypeSetup.aspx" Text="Test Type Setup" Width="150px" /> 
               </td> 
               <td class="auto-style2">
                    <asp:Button ID="testSetup_Button" PostBackUrl="../UserInterface/TestSetup.aspx" CssClass="button"  runat="server" Text="Test Setup" Width="120px"/>
                </td>
                <td class="auto-style2">
                    <asp:Button ID="testRequestEntry_Button" PostBackUrl="../UserInterface/TestRequestEntry.aspx" CssClass="button" runat="server" Text="Test Request Entry" Width="170px"/>
                </td>
                <td class="auto-style1">
                    <asp:Button ID="payment_Button" PostBackUrl="../UserInterface/Payment.aspx" CssClass="button" runat="server" href="Payment.aspx" Text="Payment" Width="120px"/>
                </td>
                <td class="auto-style1">
                    <asp:Button ID="testWiseReport_Button" PostBackUrl="../UserInterface/TestWiseReport.aspx" CssClass="button" runat="server" Text="Test Wise Report" Width="150px"/>
                </td>
                 <td class="auto-style2">
                    <asp:Button ID="typeWiseReport_Button" Enabled="False" PostBackUrl="../UserInterface/TypeWiseReport.aspx" CssClass="button" runat="server" Text="Type Wise Report" Width="150px" />
                </td>
                 <td class="auto-style2">
                    <asp:Button ID="unpaidBillReport_Button" PostBackUrl="../UserInterface/UnpaidBillReport.aspx" CssClass="button" runat="server" Text="Unpaid Bill Report" Width="150px" />
                 </td>
           </tr>
        </table>
    
          <div class="container">
              <table>
                  <tr>
                     <asp:Label ID="typeWiseReportLabel" runat="server" CssClass="label" style="margin-left:200px; font-weight:bold; font-size:24px;" Text="Type Wise Report"></asp:Label>
                  </tr>
               </table>
      <table>
          <tr>
              <td>
                   <asp:Label ID="fromDateLabel" runat="server" CssClass="label" text="From Date" Width="120px"></asp:Label>
              </td>
              <td>
                  <asp:TextBox ID="fromDateTextBox" runat="server" CssClass="input" width="250px"></asp:TextBox>
              </td>
              <td></td>
          </tr>
          <tr>
              <td>
                   <asp:Label ID="toDateLabel" runat="server" CssClass="label" Style="margin-left:290px;" text="To Date" Width="120px"></asp:Label>
              </td>
              <td>
                  <asp:TextBox ID="toDateTextBox" runat="server" CssClass="input" width="250px"></asp:TextBox>
              </td>
              <td><asp:Button ID="showButton" runat="server" CssClass="button" Style="margin-top:15px; margin-left:15px;" Text="Show" OnClick="showButton_Click" /></td>
          </tr>
      </table>

                  <asp:GridView ID="GridView1" runat="server" BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black" Width="80%" style="margin-left: 10%;margin-top: 20px">
                      <FooterStyle BackColor="#CCCCCC" />
                      <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                      <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                      <RowStyle BackColor="White" />
                      <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                      <SortedAscendingCellStyle BackColor="#F1F1F1" />
                      <SortedAscendingHeaderStyle BackColor="#808080" />
                      <SortedDescendingCellStyle BackColor="#CAC9C9" />
                      <SortedDescendingHeaderStyle BackColor="#383838" />
                  </asp:GridView>
      <table>
          <tr>
              <td class="auto-style4"><asp:Button ID="pdfButton" runat="server" CssClass="button" Style="margin-top:15px; margin-left:330px;" Text="PDF" OnClick="pdfButton_Click" /></td>
              <td class="auto-style6">
                 <asp:Label ID="totalLabel" runat="server" CssClass="label" Style="margin-left:100px;" text="Total" Width="83px"></asp:Label>
              </td>
              <td class="auto-style7">
                  <asp:TextBox ID="totalTextBox" runat="server" CssClass="input" width="150px"></asp:TextBox>
              </td>
          </tr>
      </table>
    </div>
    </div>
    </form>
</body>
</html>
