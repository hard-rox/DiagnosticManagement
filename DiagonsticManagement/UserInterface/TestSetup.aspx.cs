﻿using System;
using System.Collections.Generic;
using DiagnosticManagement.BusinessLogics;
using DiagnosticManagement.DataAccess.Models;

namespace DiagnosticManagement.UserInterface
{
    public partial class TestSetup : System.Web.UI.Page
    {
        TestTypeManager _testTypeManager=new TestTypeManager();
        TestManager _testManager=new TestManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                List<TestType> allTestTypes = _testTypeManager.GetAllTestTypes();
                testTypeDropDownList.DataValueField = "TypeId";
                testTypeDropDownList.DataTextField = "TypeName";
                testTypeDropDownList.DataSource = allTestTypes;
                testTypeDropDownList.DataBind();
            }
            GridView1.DataSource = _testManager.GetAllTests();
            GridView1.DataBind();
        }

        protected void saveButton_Click(object sender, EventArgs e)
        {
            TestType testType=new TestType();
            testType.TypeId = Convert.ToInt32(testTypeDropDownList.SelectedValue);
            Test aTest=new Test(testNameTextBox.Text,testType,Convert.ToDouble(feeTextBox.Text));
            Response.Write(_testManager.AddTest(aTest));
            testNameTextBox.Text = null;
            feeTextBox.Text = null;
        }
    }
}