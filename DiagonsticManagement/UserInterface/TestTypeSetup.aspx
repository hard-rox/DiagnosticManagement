﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestTypeSetup.aspx.cs" Inherits="DiagnosticManagement.UserInterface.TestTypeSetup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Care BITM|Test Type Setup</title>
    <link href="Style/Theme.css" rel="stylesheet" />
</head>
<body style="background-image: url('../resourses/images.jpg') " >
    <form id="form1" runat="server">
    <p class="p">BITM Care Bill Management System</p>
    <div class="box">
        <table>
            <tr>
                <td class="auto-style2">
                    <asp:Button ID="home_Button" PostBackUrl="../UserInterface/Home.aspx" CssClass="button" runat="server" Text="Home" Width="80px" style="text-align:left;"/>
                </td>
                <td class="auto-style2">
                    <asp:Button ID="testTypesetup_Button" Enabled="False" CssClass="button"  runat="server" href="TestTypeSetup.aspx" Text="Test Type Setup" Width="150px" /> 
               </td> 
               <td class="auto-style2">
                    <asp:Button ID="testSetup_Button" PostBackUrl="../UserInterface/TestSetup.aspx" CssClass="button"  runat="server" Text="Test Setup" Width="120px"/>
                </td>
                <td class="auto-style2">
                    <asp:Button ID="testRequestEntry_Button" PostBackUrl="../UserInterface/TestRequestEntry.aspx" CssClass="button" runat="server" Text="Test Request Entry" Width="170px"/>
                </td>
                <td class="auto-style1">
                    <asp:Button ID="payment_Button" PostBackUrl="../UserInterface/Payment.aspx" CssClass="button" runat="server" href="Payment.aspx" Text="Payment" Width="120px"/>
                </td>
                <td class="auto-style1">
                    <asp:Button ID="testWiseReport_Button" PostBackUrl="../UserInterface/TestWiseReport.aspx" CssClass="button" runat="server" Text="Test Wise Report" Width="150px"/>
                </td>
                 <td class="auto-style2">
                    <asp:Button ID="typeWiseReport_Button" PostBackUrl="../UserInterface/TypeWiseReport.aspx" CssClass="button" runat="server" Text="Type Wise Report" Width="150px" />
                </td>
                 <td class="auto-style2">
                    <asp:Button ID="unpaidBillReport_Button" PostBackUrl="../UserInterface/UnpaidBillReport.aspx" CssClass="button" runat="server" Text="Unpaid Bill Report" Width="150px" />
                 </td>
           </tr>
        </table>
         <div class="container">
             <table>
                 <tr>
            <asp:Label ID="testTypeSetupLabel" runat="server" CssClass="label" style="margin-left:200px; font-weight:bold; font-size:24px;" Text="Test Type Setup"></asp:Label>
                  </tr>
             </table>
            <table class="auto-style1">
                <tr>
                    <td class="auto-style5">
                        <asp:Label ID="typeNameLabel" runat="server" CssClass="label" Text="Type Name" Width="100px"></asp:Label>
                    </td>
                    <td class="auto-style6">
                        <asp:TextBox ID="typeNameTextBox" runat="server" CssClass="input" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                   <tr>
                     <td class="auto-style5">&nbsp;</td>
                     <td class="auto-style6">
                         <asp:Button ID="saveButton" runat="server" CssClass="button" Style="margin-left:230px;margin-top:10px;" Text="Save" OnClick="saveButton_Click" />
                     </td>
                   </tr>
            </table>
                <asp:GridView ID="GridView1" runat="server" BackColor="White" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px" CellPadding="3" CellSpacing="1" GridLines="None" Width="80%" style="margin-left: 10%; margin-top: 20px">
                    <Columns>
                        <asp:TemplateField HeaderText="SL">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                      <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                      <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
                      <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                      <RowStyle BackColor="#DEDFDE" ForeColor="Black" />
                      <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                      <SortedAscendingCellStyle BackColor="#F1F1F1" />
                      <SortedAscendingHeaderStyle BackColor="#594B9C" />
                      <SortedDescendingCellStyle BackColor="#CAC9C9" />
                      <SortedDescendingHeaderStyle BackColor="#33276A" />
                  </asp:GridView>
        </div>
    </div>
    </form>
</body>
</html>
