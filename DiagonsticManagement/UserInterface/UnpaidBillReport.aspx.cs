﻿using System;
using System.Data;
using System.IO;
using System.Web;
using DiagnosticManagement.BusinessLogics;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace DiagnosticManagement.UserInterface
{
    public partial class UnpaidBillReport : System.Web.UI.Page
    {
        BillManager _billManager=new BillManager();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void showButton_Click(object sender, EventArgs e)
        {
            DataTable reportTable =
            _billManager.GetUnpaidBillReport(DateTime.Parse(fromDateTextBox.Text), DateTime.Parse(toDateTextBox.Text));
            GridView1.DataSource = reportTable;
            GridView1.DataBind();

            double totalAmount = 0;
            foreach (DataRow row in reportTable.Rows)
            {
                totalAmount += Convert.ToDouble(row["DueAmmount"]);
            }
            totalTextBox.Text = totalAmount.ToString("#,###.##");
        }

        protected void pdfButton_Click(object sender, EventArgs e)
        {
            DataTable reportTable =
                _billManager.GetUnpaidBillReport(DateTime.Parse(fromDateTextBox.Text), DateTime.Parse(toDateTextBox.Text));
            GridView1.DataSource = reportTable;
            GridView1.DataBind();

            double totalAmount = 0;
            foreach (DataRow row in reportTable.Rows)
            {
                totalAmount += Convert.ToDouble(row["DueAmmount"]);
            }
            GenPdf("Unpaid Bill Report From " + fromDateTextBox.Text + " to " + toDateTextBox.Text + "\n\n",
            reportTable, "Total Ammount is " + totalAmount.ToString("#,###.##") + " TK.");

        }

        protected void GenPdf(string head, DataTable dt, string foot)
        {
            Document export = new Document(PageSize.A4, 10, 10, 30, 30);
            MemoryStream memoryStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(export, memoryStream);
            export.Open();
            Paragraph heading = new Paragraph(head);
            heading.Alignment = Element.ALIGN_CENTER;
            export.Add(heading);

            PdfPTable table = new PdfPTable(dt.Columns.Count);
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                table.AddCell(new Phrase(dt.Columns[i].ColumnName));
            }
            table.HeaderRows = 1;

            foreach (DataRow variable in dt.Rows)
            {
                for (int i = 0; i < dt.Columns.Count; i++)
                    table.AddCell(new Phrase(variable[i].ToString()));
            }

            export.Add(table);
            Paragraph total = new Paragraph(foot);
            total.Alignment = Element.ALIGN_RIGHT;
            export.Add(total);
            export.Close();

            byte[] bytes = memoryStream.ToArray();
            memoryStream.Close();
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=Report.pdf");
            Response.ContentType = "application/pdf";
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(bytes);
            Response.End();
            Response.Close();
        }
    }
}