﻿using System;
using System.Data;
using DiagnosticManagement.DataAccess.Gateway;
using DiagnosticManagement.DataAccess.Models;

namespace DiagnosticManagement.BusinessLogics
{
    public class BillManager
    {
        BillGateway _billGateway=new BillGateway();
        PatientGateway _patientGateway=new PatientGateway();
        public void SetBill(Bill bill)
        {
            _patientGateway.PatientTest(bill.Patient, bill.Patient.Tests);
            _billGateway.SetBill(bill);
        }

        public Bill GetBillByNo(string no)
        {
            return _billGateway.GetBillByNo(no);
        }

        public String PayBill(Bill bill, double ammount)
        {
            if (ammount > bill.DueAmmount) return "You cant pay more than due.";
            bill.PayBill(ammount);
            if (_billGateway.PayBill(bill, ammount)) return "Bill paid";
            return "Error....";
        }

        public DataTable GetUnpaidBillReport(DateTime from, DateTime to)
        {
            return _billGateway.UnpaidBillReport(from, to);
        }
    }
}