﻿using System;

namespace DiagnosticManagement.DataAccess.Models
{
    [Serializable]
    public class Bill
    {
        public int Id { get; set; }
        public string BillNo { get; set; }
        public Patient Patient { get; set; }
        public DateTime BillDate { get; set; }
        public double TotalAmmount { get; set; }
        public double PaidAmmount { get; set; }
        public double DueAmmount { get; set; }

        public Bill()
        {
            
        }
        public Bill(Patient patient)
        {
            BillNo = DateTime.Now.ToString("yyyyMMddHHmmss");
            Patient = patient;
            BillDate = DateTime.Now;
            TotalAmmount = patient.TotalFee();
            PaidAmmount = 0;
            DueAmmount = TotalAmmount;
        }

        public void PayBill(double ammount)
        {
            PaidAmmount += ammount;
            DueAmmount -= ammount;
        }
    }
}