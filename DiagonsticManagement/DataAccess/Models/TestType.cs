﻿using System;

namespace DiagnosticManagement.DataAccess.Models
{
    [Serializable]
    public class TestType
    {
        public int TypeId { set; get; }
        public string TypeName { set; get; }
    }
}