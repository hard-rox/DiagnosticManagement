﻿using System;
using System.Collections.Generic;

namespace DiagnosticManagement.DataAccess.Models
{
    [Serializable]
    public class Patient
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Dob { get; set; }
        public string Contact { get; set; }
        public List<Test> Tests { get; set; }

        public Patient()
        {
            Tests=new List<Test>();
        }

        public double TotalFee()
        {
            double fee = 0;
            foreach (Test test in Tests)
            {
                fee += test.Fee;
            }
            return fee;
        }
    }
}