﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DiagnosticManagement.DataAccess.Models;

namespace DiagnosticManagement.DataAccess.Gateway
{
    public class TestTypeGateway
    {
        public bool AddTestType(TestType testType)
        {
            string query = "INSERT INTO TestType (TypeName) VALUES('"+testType.TypeName+"');";
            if (ConnectionGateway.DiagosoticConnection.State != ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Open();
            SqlCommand addType = new SqlCommand(query, ConnectionGateway.DiagosoticConnection);
            int rowCount = addType.ExecuteNonQuery();
            if (ConnectionGateway.DiagosoticConnection.State == ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Close();
            if (rowCount>0) return true;
            return false;
        }

        public List<TestType> GetAllTestTypes()
        {
            List<TestType> testTypes=new List<TestType>();

            string query = "SELECT * FROM TestType order by TypeName;";
            if (ConnectionGateway.DiagosoticConnection.State != ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Open();
            SqlCommand getCommand=new SqlCommand(query, ConnectionGateway.DiagosoticConnection);
            SqlDataReader getReader = getCommand.ExecuteReader();
            if (getReader.HasRows)
            {
                while (getReader.Read())
                {
                    TestType testType=new TestType();
                    testType.TypeId = (int) getReader["ID"];
                    testType.TypeName = getReader["TypeName"].ToString();
                    testTypes.Add(testType);
                }
            }
            getReader.Close();
            if (ConnectionGateway.DiagosoticConnection.State == ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Close();
            return testTypes;
        }

        public TestType GetTypeById(int id)
        {
            TestType testType=new TestType();

            string query = "SELECT * FROM TestType WHERE ID="+id+";";
            if (ConnectionGateway.DiagosoticConnection.State != ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Open();
            SqlCommand getCommand = new SqlCommand(query, ConnectionGateway.DiagosoticConnection);
            SqlDataReader gettype = getCommand.ExecuteReader();
            if (gettype.HasRows)
            {
                while (gettype.Read())
                {
                    testType.TypeId = (int)gettype["ID"];
                    testType.TypeName = gettype["TypeName"].ToString();
                }
            }
            gettype.Close();
            if (ConnectionGateway.DiagosoticConnection.State == ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Close();
            return testType;
        }

        public DataTable GetTypeWiseReport(DateTime from, DateTime to)
        {
            DataTable reportTable = new DataTable();
            string query = "SELECT * FROM GetTypeWiseReport ('" + from.ToString("yyyy-MM-dd") + "', '" + to.ToString("yyyy-MM-dd") + "');";
            if (ConnectionGateway.DiagosoticConnection.State != ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Open();
            SqlCommand report = new SqlCommand(query, ConnectionGateway.DiagosoticConnection);
            SqlDataReader reader = report.ExecuteReader();
            reportTable.Load(reader);
            reader.Close();
            if (ConnectionGateway.DiagosoticConnection.State == ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Close();
            return reportTable;
        }

        public bool IsDuplicate(TestType testType)
        {

            string query = "SELECT * FROM TestType WHERE TypeName='" + testType.TypeName + "';";
            if (ConnectionGateway.DiagosoticConnection.State != ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Open();
            SqlCommand getCommand = new SqlCommand(query, ConnectionGateway.DiagosoticConnection);
            SqlDataReader getReader = getCommand.ExecuteReader();
            if (getReader.HasRows)
            {
                getReader.Close();
                if (ConnectionGateway.DiagosoticConnection.State == ConnectionState.Open)
                    ConnectionGateway.DiagosoticConnection.Close();
                return true;
            }
            getReader.Close();
            if (ConnectionGateway.DiagosoticConnection.State == ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Close();
            return false;
        }
    }
}