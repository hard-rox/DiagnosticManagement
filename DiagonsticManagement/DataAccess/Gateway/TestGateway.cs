﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DiagnosticManagement.DataAccess.Models;

namespace DiagnosticManagement.DataAccess.Gateway
{
    public class TestGateway
    {
        public bool AddTest(Test test)
        {
            string query = "INSERT INTO Test(TestName, Fee, TestType) VALUES('" + test.Name + "', " + test.Fee + ", " +
                           test.Type.TypeId + ");";
            if (ConnectionGateway.DiagosoticConnection.State != ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Open();
            SqlCommand addtestCommand=new SqlCommand(query, ConnectionGateway.DiagosoticConnection);
            int rowCount = addtestCommand.ExecuteNonQuery();
            if (ConnectionGateway.DiagosoticConnection.State == ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Close();
            if (rowCount > 0) return true;
            return false;
        }

        public List<Test> GetAllTests()
        {
            List<Test> tests=new List<Test>();

            string query = "SELECT * FROM TestWithTestType order by TestName;";
            if (ConnectionGateway.DiagosoticConnection.State != ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Open();
            SqlCommand getCommand = new SqlCommand(query, ConnectionGateway.DiagosoticConnection);
            SqlDataReader getReader = getCommand.ExecuteReader();
            if (getReader.HasRows)
            {
                while (getReader.Read())
                {
                    TestType testType=new TestType();
                    testType.TypeId = (int) getReader["TypeId"];
                    testType.TypeName = getReader["TypeName"].ToString();

                    Test test = new Test(
                        getReader["TestName"].ToString(), testType, Convert.ToDouble(getReader["Fee"]));
                    test.Id = Convert.ToInt32(getReader["ID"]);

                    tests.Add(test);
                }
            }
            getReader.Close();
            if (ConnectionGateway.DiagosoticConnection.State == ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Close();
            return tests;
        }

        public DataTable GetTestWiseReport(DateTime from, DateTime to)
        {
            DataTable reportTable=new DataTable();
            string query = "SELECT * FROM GetTestWiseReport ('"+from.ToString("yyyy-MM-dd")+"', '"+to.ToString("yyyy-MM-dd")+"');";
            if (ConnectionGateway.DiagosoticConnection.State != ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Open();
            SqlCommand report=new SqlCommand(query,ConnectionGateway.DiagosoticConnection);
            SqlDataReader reader = report.ExecuteReader();
            reportTable.Load(reader);
            reader.Close();
            if (ConnectionGateway.DiagosoticConnection.State == ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Close();
            return reportTable;
        }

        public Test GetTestById(int id)
        {
            Test test=new Test();
            string query = "select * from Test where Test.ID="+id+";";
            if (ConnectionGateway.DiagosoticConnection.State != ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Open();
            SqlCommand getCommand=new SqlCommand(query,ConnectionGateway.DiagosoticConnection);
            SqlDataReader reader = getCommand.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    test.Id = Convert.ToInt32(reader["ID"]);
                    test.Name = reader["TestName"].ToString();
                    test.Fee = Convert.ToDouble(reader["Fee"]);
                }
                reader.Close();
                if (ConnectionGateway.DiagosoticConnection.State == ConnectionState.Open)
                    ConnectionGateway.DiagosoticConnection.Close();
                return test;
            }
            reader.Close();
            if (ConnectionGateway.DiagosoticConnection.State == ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Close();
            return null;
        }

        public bool IsDuplicate(Test test)
        {
            string query = "SELECT * FROM TestWithTestType WHERE TestName='"+test.Name+"';";
            if (ConnectionGateway.DiagosoticConnection.State != ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Open();
            SqlCommand getCommand = new SqlCommand(query, ConnectionGateway.DiagosoticConnection);
            SqlDataReader getReader = getCommand.ExecuteReader();
            if (getReader.HasRows)
            {
                getReader.Close();
                if (ConnectionGateway.DiagosoticConnection.State == ConnectionState.Open)
                    ConnectionGateway.DiagosoticConnection.Close();
                return true;
            }
            getReader.Close();
            if (ConnectionGateway.DiagosoticConnection.State == ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Close();
            return false;
        }
    }
}